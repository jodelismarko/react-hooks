import React, { useState, useEffect } from 'react';
import { useForm } from 'react-hook-form'
import RepoDataService from "../service/repositories"
import './styles.css';

export default function App() {

  const [repositorio, setRepositorio] = useState([]);
  const [location, setLocation] = useState({});
  const { register, handleSubmit, setValue } = useForm();

  useEffect(() => {
    navigator.geolocation.watchPosition(handlePositionReceived);
    refreshCourses();
  }, []);


  function handlePositionReceived(coordinates){
    const {latitude, longitude} = coordinates.coords;
    setLocation({latitude, longitude});
    console.log(coordinates.coords);
  }

  useEffect(() => {
    const filtered = repositorio.filter(repo => repo.favorito);
    const favicon = document.getElementById('favicon')
    document.title = `voce tem ${filtered.length} favoritos`;
    if (filtered.length < 5 ? favicon.href = 'logo392.png' : favicon.href = 'logo492.png' );
  }, [repositorio]);


  function handleFavorite(id) {
    const newRepositorio = repositorio.map(repo => {
      return repo.id === id ? { ...repo, favorito: !repo.favorito } : repo;
    });
    setRepositorio(newRepositorio);
  }

  function deleteRepository(id) {
    RepoDataService.deleteCourse(id)
      .then(response => {
        refreshCourses()
      })
  }
  
  function editarRepository(id) {
    const filtered = repositorio.filter(repo => repo.id === id)
    const data = filtered[0]
    setValue([
      { id: data.id },
      { name: data.name },
    ])
  }

  function refreshCourses() {
    RepoDataService.retrieveAllCourses()
      .then(response => {
        setRepositorio(response.data)
      })
  }

  function onSubmit(repo, e) {
    e.target.reset();
    const filtered = repositorio.filter(repos => repos.id === repo.id)

   if (filtered.length === 0){
      RepoDataService.createCourse(repo)
      .then(response => {
        refreshCourses()
      })
    } else {
      RepoDataService.updateCourse(repo.id, repo)
      .then(response => {
        refreshCourses()
      })      
    }
  }

  return (
    <div>
      Latitude: {location.latitude} <br/>
      Longitude: {location.longitude}
      <ul>
        {repositorio.map(repo => (
          <li key={repo.id}>
            {repo.name}
            {repo.favorito && <span>(Favorito)</span>}
            <button onClick={() => handleFavorite(repo.id)}>Favoritar</button>
            <button onClick={() => deleteRepository(repo.id)}>Deletar</button>
            <button onClick={() => editarRepository(repo.id)}>Editar</button>
          </li>
        ))}
      </ul>
      <div>
        <form onSubmit={handleSubmit(onSubmit)}>
          ID: <input name="id" ref={register} /><br />
          Nome: <input name="name" ref={register} />
          <input type="submit" />
        </form>
      </div>

    </div>
  );
}

