import axios from 'axios'

const COURSE_API_URL = 'http://localhost:4200'

class RepoDataService {

    async retrieveAllCourses() {
        //console.log('executed service')
        const response = await axios.get(`${COURSE_API_URL}/repositories`)
        return response;
    }

    async retrieveCourse(id) {
        //console.log('executed service')
        const response = await axios.get(`${COURSE_API_URL}/repositories/${id}`);
        return response;
    }

    deleteCourse(id) {
        //console.log('executed service')
        return axios.delete(`${COURSE_API_URL}/repositories/${id}`);
    }

    updateCourse(id, course) {
        //console.log('executed service')
        return axios.put(`${COURSE_API_URL}/repositories/${id}`, course);
    }

    createCourse(course) {
        //console.log('executed service')
        return axios.post(`${COURSE_API_URL}/repositories/`, course);
    }

}

export default new RepoDataService()
